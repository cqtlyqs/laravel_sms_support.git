<?php

namespace Ktnw\sms\Providers;

use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes($this->getPublishFiles());

        $this->mergeConfigFrom(__DIR__ . '/../../resources/config/smsConfig.php', 'smsConfig');
    }

    private function getPublishFiles(): array
    {
        $data       = $this->publishData();
        $srcData    = array_column($data, "src");
        $targetData = array_column($data, "target");
        $publishes  = [];
        for ($i = 0; $i < count($srcData); $i++) {
            $publishes[$srcData[$i]] = $targetData[$i];
        }
        return $publishes;
    }

    private function publishData(): array
    {
        return [
            ['src' => __DIR__ . '/../../resources/config/smsConfig.php', 'target' => config_path('smsConfig.php')],
            ['src' => __DIR__ . '/../../resources/config/captcha.php', 'target' => config_path('captcha.php')],
            ['src' => __DIR__ . '/../controller/SmsController.php', 'target' => app_path("Http/Controllers/Api/SmsController.php")],
            ['src' => __DIR__ . '/../Console/Commands/SmsSupportCommand.php', 'target' => app_path("Console/Commands/SmsSupportCommand.php")],
            ['src' => __DIR__ . '/../../database/migrations/2022_01_21_135810_create_sms_log.php', 'target' => database_path("migrations/2022_01_21_135810_create_sms_log.php")],
        ];
    }


}