<?php

namespace Ktnw\sms\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SmsLog.
 *
 * @package namespace App\Models;
 */
class SmsLog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable   = [];
    protected $hidden     = [];
    protected $table      = 'sms_log';
    public    $timestamps = FALSE;

}
